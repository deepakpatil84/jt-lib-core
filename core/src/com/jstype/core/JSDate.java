/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.core;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;

@NativeNonInstanciable(proto="Date")
public class JSDate {

	protected JSDate() {

	}

	public static native JSDate create()
	/*-{
	  return new Date();
	}-*/;

	public static native JSDate create(double ms)
	/*-{
	  return new Date(ms);
	}-*/;

	public static native JSDate create(int year, int month)
	/*-{
	  return new Date(year, month);
	}-*/;

	public static native JSDate create(int year, int month, int dayOfMonth)
	/*-{
	  return new Date(year, month, dayOfMonth);
	}-*/;

	public static native JSDate create(int year, int month, int dayOfMonth,
	        int hours)
	/*-{
	  return new Date(year, month, dayOfMonth, hours);
	}-*/;

	public static native JSDate create(int year, int month, int dayOfMonth,
	        int hours, int minutes)
	/*-{
	  return new Date(year, month, dayOfMonth, hours, minutes);
	}-*/;

	public static native JSDate create(int year, int month, int dayOfMonth,
	        int hours, int minutes, int seconds)
	/*-{
	  return new Date(year, month, dayOfMonth, hours, minutes, seconds);
	}-*/;

	public static native JSDate create(int year, int month, int dayOfMonth,
	        int hours, int minutes, int seconds, int millis)
	/*-{
	  return new Date(year, month, dayOfMonth, hours, minutes, seconds, millis);
	}-*/;

	public static native JSDate create(String dateString)
	/*-{
	  return new Date(dateString);
	}-*/;

	public static native double parse(String dateString)
	/*-{
	  return Date.parse(dateString);
	}-*/;

	public static native double UTC(int year, int month, int dayOfMonth,
	        int hours, int minutes, int seconds, int millis)
	/*-{
	  return Date.UTC(year, month, dayOfMonth, hours, minutes, seconds, millis);
	}-*/;

	@NoJavaScript
	public final native int getDate();

	@NoJavaScript
	public final native int getDay();

	@NoJavaScript
	public final native int getFullYear();

	@NoJavaScript
	public final native int getHours();

	@NoJavaScript
	public final native int getMilliseconds();

	@NoJavaScript
	public final native int getMinutes();

	@NoJavaScript
	public final native int getMonth();

	@NoJavaScript
	public final native int getSeconds();

	@NoJavaScript
	public final native double getTime();

	@NoJavaScript
	public final native int getTimezoneOffset();

	@NoJavaScript
	public final native int getUTCDate();

	@NoJavaScript
	public final native int getUTCDay();

	@NoJavaScript
	public final native int getUTCFullYear();

	@NoJavaScript
	public final native int getUTCHours();

	@NoJavaScript
	public final native int getUTCMilliseconds();

	@NoJavaScript
	public final native int getUTCMinutes();

	@NoJavaScript
	public final native int getUTCMonth();

	@NoJavaScript
	public final native int getUTCSeconds();

	@NoJavaScript
	public final native double setDate(int dayOfMonth);

	@NoJavaScript
	public final native double setFullYear(int year);

	@NoJavaScript
	public final native double setFullYear(int year, int month);

	@NoJavaScript
	public final native double setFullYear(int year, int month, int day);

	@NoJavaScript
	public final native double setHours(int hours);

	@NoJavaScript
	public final native double setHours(int hours, int mins);

	@NoJavaScript
	public final native double setHours(int hours, int mins, int secs);

	@NoJavaScript
	public final native double setHours(int hours, int mins, int secs, int ms);

	@NoJavaScript
	public final native double setMinutes(int minutes);

	@NoJavaScript
	public final native double setMinutes(int minutes, int seconds);

	@NoJavaScript
	public final native double setMinutes(int minutes, int seconds, int millis);

	@NoJavaScript
	public final native double setMonth(int month);

	@NoJavaScript
	public final native double setMonth(int month, int dayOfMonth);

	@NoJavaScript
	public final native double setSeconds(int seconds);

	@NoJavaScript
	public final native double setSeconds(int seconds, int millis);

	@NoJavaScript
	public final native double setTime(double milliseconds);

	@NoJavaScript
	public final native double setUTCDate(int dayOfMonth);

	@NoJavaScript
	public final native double setUTCFullYear(int year);

	@NoJavaScript
	public final native double setUTCFullYear(int year, int month);

	@NoJavaScript
	public final native double setUTCFullYear(int year, int month, int day);

	@NoJavaScript
	public final native double setUTCHours(int hours);

	@NoJavaScript
	public final native double setUTCHours(int hours, int mins);

	@NoJavaScript
	public final native double setUTCHours(int hours, int mins, int secs);

	@NoJavaScript
	public final native double setUTCHours(int hours, int mins, int secs, int ms);

	@NoJavaScript
	public final native double setUTCMinutes(int minutes);

	@NoJavaScript
	public final native double setUTCMinutes(int minutes, int seconds);

	@NoJavaScript
	public final native double setUTCMinutes(int minutes, int seconds,
	        int millis);

	@NoJavaScript
	public final native double setUTCMonth(int month);

	@NoJavaScript
	public final native double setUTCMonth(int month, int dayOfMonth);

	@NoJavaScript
	public final native double setUTCSeconds(int seconds);

	@NoJavaScript
	public final native double setUTCSeconds(int seconds, int millis);

	@NoJavaScript
	public final native String toDateString();

	@NoJavaScript
	public final native String toLocaleDateString();

	@NoJavaScript
	public final native String toLocaleString();

	@NoJavaScript
	public final native String toLocaleTimeString();

	@NoJavaScript
	public final native String toTimeString();

	@NoJavaScript
	public final native String toUTCString();

	@NoJavaScript
	public final native double valueOf();

}