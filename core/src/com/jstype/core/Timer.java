/*
 * Copyright 2013 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.core;

/**
 * Wraps native setTimeout and setInterval function
 * <pre>
 *  Timer t=new new Timer(){
 *  	void run(){
 *  		//your code goes here;
 *  	}
 *  }
 *  t.schedule(100);
 *  //or
 *  t.scheduleRepeating(100);
 * </pre>
 * 
 * You can schedule one Timer object multiple times , you will get the id of last schedule through getId method
 * 
 * @author Deepak Patil
 * 
 */

public abstract class Timer {

	private int id;
	private boolean repeating;

	public Timer() {
		repeating = false;
		id = -1;
	}

	/**
	 * Returns id returned by native methods i.e. setTimeout or setInterval
	 * This id can be used to cancel the timer
	 * 
	 * @return id returned by native method
	 */
	public int getId() {
		return id;
	}

	/**
	 * <code>true</code> if Timer is repeating
	 * 
	 * @return
	 */
	public boolean isRepeating() {
		return repeating;
	}

	/**
	 * Implement this method to add functionality, gets called on timer event
	 */
	public abstract void run();

	/**
	 * Clears the interval for id
	 * @param id id of the interval
	 */
	private static native void clearInterval(int id)
	/*-{
	    clearInterval(id);
	}-*/;

	/**
	 * Clears the timeout for id
	 * @param id id of the timeout
	 */
	private static native void clearTimeout(int id)
	/*-{
	    clearTimeout(id);
	}-*/;

	/**
	 * Calls the native setTimeout method for specified delay
	 * 
	 * @param timer
	 * @param delay
	 * @return
	 */
	private static native int setTimout(Timer timer, int delay)
	/*-{
	    return setTimeout(function(){ timer.@com.jstype.core.Timer::fire()(); },delay);
	}-*/;

	/**
	 * Calls the native setInterval method for specified delay
	 * @param timer
	 * @param delay
	 * @return
	 */
	private static native int setInterval(Timer timer, int delay)
	/*-{
	    return setInterval(function(){ timer.@com.jstype.core.Timer::fire()(); },delay);
	}-*/;

	/**
	 * Schedules the timer for specified delay
	 * 
	 * @param delay time in milliseconds
	 */
	public void schedule(int delay) {
		if (id == -1) {
			id = setTimout(this, delay);
			repeating = false;
		}

	}

	/**
	 * Schedules the time repeating for specified delay
	 * @param delay
	 */
	public void scheduleRepeating(int delay) {
		if (id == -1) {
			id = setInterval(this, delay);
			repeating = true;
		}
	}

	/**
	 * Cancels the timer 
	 */
	public void cancel() {
		if (id == -1) {
			return;
		}
		if (repeating) {
			clearInterval(id);
		} else {
			clearTimeout(id);
		}
		id = -1;
	}

	/**
	 * Calls run method
	 */
	private void fire() {
		if (!repeating) {
			id = -1;
		}
		run();
	}

}
