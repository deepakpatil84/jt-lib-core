package com.jstype.core;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;

@NativeNonInstanciable(proto="Object")
public class JSObject {
	
	
	
	@NoJavaScript
	public native boolean hasOwnProperty(String name);

	public final native void setProperty(String name, boolean value)
	/*- {
		this[name]=value;
	} -*/;

	public final native void setProperty(String name, int value)
	/*- {
		this[name]=value;
	} -*/;

	public final native void setProperty(String name, Object value)
	/*- {
		this[name]=value;
	} -*/;

	public final native void setProperty(String name, double value)
	/*- {
		this[name]=value;
	} -*/;

	public final native void setProperty(String name, String value)
	/*- {
		this[name]=value;
	} -*/;

	public native int getPropertyInt(String name)
	/*-{
	 	return this[name];
	 }-*/;

	public native JSObject getPropertyJSObject(String name)
	/*-{
		return this[name];
	}-*/;
	
	public final native boolean getPropertyBoolean(String name)
	/*- {
		return !!this[name];
	} -*/;

	public final native double getPropertyDouble(String name)
	/*- {
		return parseFloat(this[name]) || 0.0;
	} -*/;

	public final native Object getPropertyObject(String name)
	/*- {
		return this[name] || null;
	} -*/;

	public final native String getPropertyString(String name)
	/*- {
		return this[name] || null;
	} -*/;

}
